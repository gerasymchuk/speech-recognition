package com.trembit.speechrecognition.app;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;

public class SettingsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        FragmentManager mFragmentManager = getFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager
                .beginTransaction();
        SettingsFragment settingsFragment = new SettingsFragment();
        mFragmentTransaction.replace(android.R.id.content, settingsFragment);
        mFragmentTransaction.commit();

    }

    public static class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

        public static final String KEY_PREF_SHOW_GOOGLE_DIALOG = "pref_showGoogleSpeechRecognitionDialog";

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);

            onSharedPreferenceChanged(getPreferenceManager().getSharedPreferences(), KEY_PREF_SHOW_GOOGLE_DIALOG);
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

        }

        @Override
        public void onPause() {
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
            super.onPause();
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                              String key) {
            if (key.equals(KEY_PREF_SHOW_GOOGLE_DIALOG)) {
                Preference pref = findPreference(key);
                // Set summary to be the user-description for the selected value
                //pref.setSummary(sharedPreferences.getString(key, ""));
            }
        }
    }
}
