package com.trembit.speechrecognition.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "SpeechRecognitionApp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnBuildIn = (Button) findViewById(R.id.btnGoogleSpeechRecognition);
        Button btnSphinx = (Button) findViewById(R.id.btnSphinx);

        // hide the action bar
        ActionBar actionBar = getSupportActionBar();
        if (null != actionBar) {
            actionBar.setElevation(0);
        }

        btnBuildIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, GoogleSpeechRecognitionActivity.class));
            }
        });

        btnSphinx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, PocketSphinxActivity.class));
            }
        });
    }
}
